# CBS2.0
Koncept nowego CBSa. Podejście by wykorzystać bardziej popularny framework i przepiąć się od razy na _Python 3.5+_.
Kolejne założenie to wykorzystać nową bibliotekę _vue.js_ dla lepszej responsywności i łatwiejszego utrzymania frontendu.
